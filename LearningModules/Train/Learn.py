from __future__ import annotations
import os
from typing import Dict, Any
import mlflow
import pytorch_lightning as pl
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.callbacks import ModelCheckpoint

from ETL.DataLoader import GetDataLoaders
from LearningModules.Train.TrainingLoop import LitModule

def LogParameters(config: Dict[str, Dict[str, Any]] | Any) -> None:
    for key, value in config.items():
        if isinstance(value, Dict):
            mlflow.log_params(value)
        else:
            mlflow.log_param(key, value)

if __name__ == "__main__":
    SAVINGFOLDER = os.path.join("LearningModules", "TrainingCheckpoints")
    os.makedirs(SAVINGFOLDER, exist_ok=True)

    CONFIG: Dict[str, Dict[str, Any]] = {
        'modelParameters':{
            'nstack': 2,
            'nhglass': 4,
            'inp_dim': 256,
            'oup_dim': 9,
            'increase': 0,
            'lossName': 'L2',
            'reduction': 'sum'
        },
        'trainParameters':{
            'optimizer': 'Adam',
            'learningRate': 5e-4,
            'weightDecay': 1e-5,
            'scheduler': 'StepLR'
        },
        'setupConfig': {
            'maxEpochs': 100,
            # 'earlyStopPatience': 20
        },
        'inferenceConfig':{
            'pck_threshold': 16,
            'heatmapSelection': 'last'
        }
    }
    mlflow.set_tracking_uri("file:///C:/Users/mcisse/Documents/Code/sheep-pain/mlruns")
    mlflow.set_experiment("KeyPointLocalization")

    DEVICE = 0
    RUN_NAME = None
    with mlflow.start_run(run_name=RUN_NAME) as run:
        LogParameters(CONFIG)
        plmodel = LitModule(CONFIG)
        trainDataLoader, validDataLoader = GetDataLoaders('Split')
        
        early_stop_callback = EarlyStopping(monitor='val_pcks', min_delta=0.000, patience=10, verbose=True, mode='max')
        ckptPath = os.path.join(SAVINGFOLDER, "checkpoints", run.info.run_id)
        os.makedirs(ckptPath, exist_ok=True)
        checkpointCallback = ModelCheckpoint(monitor="val_pcks", filename="{epoch}-{val_pcks:.2f}", dirpath=ckptPath, mode="max", save_top_k=5)

        trainer = pl.Trainer(
            gpus=[DEVICE], 
            max_epochs=CONFIG['setupConfig']['maxEpochs'], 
            callbacks=[early_stop_callback, checkpointCallback], 
            default_root_dir=ckptPath,
            )
        #
        try: 
            trainer.fit(plmodel, train_dataloader=trainDataLoader, val_dataloaders=validDataLoader)
        except RuntimeError:
            pass
        finally:
            if trainer.checkpoint_callback is not None: 
                ckptPath = checkpointCallback.best_model_path
                bestModel = LitModule.load_from_checkpoint(ckptPath, configParameters=CONFIG)
                mlflow.pytorch.log_model(bestModel.model, "SavedModels/bestModel.pth")
                mlflow.pytorch.log_model(plmodel.model, "SavedModels/trainedModel.pth")
