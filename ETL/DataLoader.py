from __future__ import annotations
import math
import os
from typing import List, Tuple
import numpy as np
import torch
# from torchvision.transforms.functional import resize
from PIL import Image
from torch.utils.data import DataLoader, Dataset
from torchvision.transforms import transforms
from scipy.ndimage import gaussian_filter
from scipy.ndimage import grey_dilation
INTERPOLATION_MODE = transforms.InterpolationMode.BILINEAR

def _NormalizedGaussianFilter(inputImage: np.ndarray, sigma: float = 1.5, mode: str = 'nearest') -> np.ndarray:
    heatmap = gaussian_filter(inputImage, sigma=sigma, mode=mode)
    return (heatmap - heatmap.min()) / (heatmap.max() - heatmap.min())

def _ExpandZoneToFocus(inputImage, size: int = 15):
    return grey_dilation(inputImage, size=size)

def RoundUpToEven(f):
    return math.ceil(f / 2.) * 2


class Loader:
    """Load image and points coordinates as numpy array

    Returns:
        _type_: _description_
    """

    def __call__(self, imagePath: str, pointsPath: str) -> Tuple[np.ndarray, np.ndarray]:
        loadedFeatures = self.LoadImageAsTensor(imagePath)
        loadedPoints = self.LoadPtsFile(pointsPath)
        return loadedFeatures, loadedPoints

    @staticmethod
    def LoadImageAsTensor(imageFilepath: str) -> np.ndarray:
        image = Image.open(imageFilepath).convert('RGB')
        return np.asarray(image)
    
    @staticmethod
    def LoadPtsFile(filepath: str) -> np.ndarray:
        """
        takes as input the path to a .pts and returns a list of tuples of floats containing the points in in the form:
        [(x_0, y_0, z_0),
        (x_1, y_1, z_1),
        ...
        (x_n, y_n, z_n)]
        Source: https://github.com/albanie/pts_loader/blob/master/pts_loader.py
        """
        with open(filepath) as f:
            rows = [rows.strip() for rows in f]

        head = rows.index('{') + 1
        tail = rows.index('}')

        rawPoints = rows[head:tail]
        coordsSet = [point.split() for point in rawPoints]

        # Convert entries from lists of strings to tuples of floats
        points = [tuple([float(point) for point in coords]) for coords in coordsSet]
        return np.array(points)


class Transformer:
    def __init__(self, featureSize: int = 256, heatmapSize: int = 64, sigma: float = 0.7) -> None:
        self.featureSize = featureSize
        self.heatmapSize = heatmapSize
        self.sigma = sigma
        self.featureTransforms = (
            transforms.ToTensor(),
            transforms.Resize(size=[self.featureSize, self.featureSize], interpolation=INTERPOLATION_MODE),
            # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        )
            
    @staticmethod
    def _GenerateRawHeatmaps(features: np.ndarray, labels: np.ndarray, fillValue: float = 1.0, sigma: float = 1.0) -> np.ndarray:
        height, width = features.shape[0], features.shape[1]
        numPoints = labels.shape[0]
        points = np.round(labels).astype(np.int64)
        heatmaps = np.zeros((numPoints, height, width), dtype=np.float32)
        heatmaps[range(numPoints), points[:, 1], points[:, 0]] = fillValue
        heatmaps = np.stack([_NormalizedGaussianFilter(heatmaps[i], sigma=sigma, mode='nearest') for i in range(numPoints)])
        return heatmaps

    def TransformFeatures(self, features: np.ndarray) -> Tuple[torch.Tensor, Tuple[float, float]]:
        # features is image of shape H, W, 3
        scales = self.heatmapSize / features.shape[1], self.heatmapSize / features.shape[0] 
        for operation in self.featureTransforms:
            features = operation(features)
        return features.float(), scales
    
    # def TransformLabels(labels: np.ndarray, features: np.ndarray, fillValue: float = 1.0, sigma: float = 2.0) -> torch.Tensor:
    def TransformLabels(self, labels: np.ndarray, scales: Tuple[float, float]) -> Tuple[torch.Tensor, torch.Tensor]:
        scaledLabels = labels * np.array(scales)
        scaledLabels = np.round(np.clip(scaledLabels, 0, self.heatmapSize - 1)).astype(np.int32)
        numPoints = scaledLabels.shape[0]
        outputShape = (numPoints, self.heatmapSize, self.heatmapSize)
        binaryLabels = np.zeros(outputShape)
        binaryLabels[range(numPoints), scaledLabels[:, 1], scaledLabels[:, 0]] = 1.0
        finalHeatmaps = np.stack([_NormalizedGaussianFilter(binaryLabels[i], sigma=self.sigma, mode='nearest') for i in range(numPoints)]) 
        intermediateHeatmaps = np.stack([_ExpandZoneToFocus(binaryLabels[i]) for i in range(numPoints)]) 
        heatmaps = np.stack([intermediateHeatmaps, finalHeatmaps])
        return torch.from_numpy(heatmaps).float(), torch.from_numpy(scaledLabels).float()

    def __call__(self, features: np.ndarray, labels: np.ndarray) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
        features, scales = self.TransformFeatures(features)
        heatmaps, points = self.TransformLabels(labels, scales)
        return features, heatmaps, points


class AnimalDataset(Dataset):
    def __init__(self, dataFolder: str, loader: Loader, transformer: Transformer) -> None:
        super().__init__()
        self.dataFilepaths = self._GetFilepaths(dataFolder)
        self.loader = loader
        self.transformer = transformer

    def __getitem__(self, index: int) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
        imagePath, pointsPath = self.dataFilepaths[0][index], self.dataFilepaths[1][index]
        rawFeatures, rawLabels = self.loader(imagePath, pointsPath)
        transformedFeatures, heatmaps, points = self.transformer(rawFeatures, rawLabels)
        return transformedFeatures, heatmaps, points

    def __len__(self) -> int:
        return len(self.dataFilepaths[0])

    @staticmethod
    def _GetFilepaths(dataFolder: str) -> Tuple[List[str], List[str]]:
        contentFolder = [os.path.join(dataFolder, data) for data in os.listdir(dataFolder)]    
        imageFilepaths = list(filter(lambda filepath: '.jpg' in filepath, contentFolder))
        pointsFilepaths = list(filter(lambda filepath: '.pts' in filepath, contentFolder))
        imageFilepaths.sort()
        pointsFilepaths.sort()
        return imageFilepaths, pointsFilepaths


def GetDataLoaders(dataFolder: str, featureSize: int = 256, heatmapSize: int = 64) -> Tuple[DataLoader, DataLoader]:
    loader = Loader()
    transformer = Transformer(featureSize, heatmapSize)
    trainFilepaths = os.path.join(dataFolder, 'Train')
    validFilepaths = os.path.join(dataFolder, 'Valid')
    trainDataset = AnimalDataset(trainFilepaths, loader, transformer)
    validDataset = AnimalDataset(validFilepaths, loader, transformer)
    return DataLoader(trainDataset, batch_size=8, shuffle=False), DataLoader(validDataset, batch_size=1)


if __name__ == "__main__":
    LOADER = Loader()
    TRANSFORMER = Transformer(featureSize=256, heatmapSize=64)
    DATASET = AnimalDataset("Split//Train", LOADER, TRANSFORMER)
    FEATURES, LABELS, _ = DATASET[8]
    print(FEATURES.shape)
    print(LABELS.shape)
