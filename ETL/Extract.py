from __future__ import annotations

import os
import shutil
from typing import Callable, List
from pathlib import Path

from sklearn.model_selection import train_test_split


def Main(dataFolder: str, extractFolder: str) -> None:
    shutil.rmtree(extractFolder, ignore_errors=True)
    os.makedirs(extractFolder)
    contentFolder = [os.path.join(dataFolder, data) for data in os.listdir(dataFolder)]    
    imageFilepaths = list(filter(lambda filepath: '.jpg' in filepath, contentFolder))
    pointsFilepaths = list(filter(lambda filepath: '.pts' in filepath, contentFolder))
    trainImages, validImages, trainPoints, validPoints = train_test_split(imageFilepaths, pointsFilepaths, test_size=0.15, shuffle=True, random_state=88)
    SaveSubsets(trainImages, trainPoints, extractFolder, 'Train')
    SaveSubsets(validImages, validPoints, extractFolder, 'Valid')


def SaveSubsets(features: List[str], labels: List[str], destFolder: str, subset: str) -> None:
    
    def MoveSingleFile(source: str, destPrefix: str) -> None:
        fileName = Path(source).name
        dest = os.path.join(destPrefix, fileName)
        shutil.copy(source, dest)
    
    def MoveMultipleFiles(moveFunction: Callable[[str, str], None], items: List[str], destFolder: str) -> None:
        for item in items:
            moveFunction(item, destFolder)

    destFolder = os.path.join(destFolder, subset)
    os.makedirs(destFolder)
    MoveMultipleFiles(MoveSingleFile, features, destFolder)
    MoveMultipleFiles(MoveSingleFile, labels, destFolder)
    

if __name__ == "__main__":
    FOLDERPATH = 'RawData'
    EXTRACT_FOLDER = 'Split'
    Main(FOLDERPATH, EXTRACT_FOLDER)
