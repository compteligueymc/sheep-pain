from typing import Dict, Any
import mlflow
import pytorch_lightning as pl
import torch
from LearningModules.Models.posenet import PoseNet
from EvaluationModules.Metric import PostProcessor, PCK
# -
# +
class LitModule(pl.LightningModule):

    def __init__(self, configParameters):
        super().__init__()

        self.model = PoseNet(**configParameters['modelParameters'])

        self.trainParameters = configParameters['trainParameters']

        self.postprocessor = PostProcessor(configParameters['inferenceConfig']['heatmapSelection'])
        self.metricFn = PCK(configParameters['inferenceConfig']['pck_threshold'])

    def training_step(self, batch, _):
        features, heatmaps, _ = batch
        # features = features[0].to(self.device)
        hmPreds = self.model(features)
        loss = self.model.calc_loss(hmPreds, heatmaps)
        return {'loss': loss}

    def training_epoch_end(self, outputs):
        trainLoss = torch.stack([x['loss'] for x in outputs]).mean()
        
        # self.log('loss', float(trainLoss), logger=False, prog_bar=True)

        mlflow.log_metrics(
            {
                'loss': float(trainLoss),
            },
            step=self.current_epoch,
        )

    def validation_step(self, batch, _):
        features, heatmaps, gtPoints = batch
        hmPreds = self.model(features)
        loss = self.model.calc_loss(hmPreds, heatmaps)

        predPoints = self.postprocessor(hmPreds)
        perJointScore = self.metricFn(predPoints, gtPoints[0])
        return {
            'valLoss': loss,
            'scores': perJointScore }

    def validation_epoch_end(self, outputs):
        valLoss = torch.stack([x['valLoss'] for x in outputs]).mean()
        valScores = torch.stack([x['scores'] for x in outputs], dim=1).mean(dim=1)
        valMetrics: Dict[str, float] = {str(key):float(value) for key, value in enumerate(valScores)}
        self.log('val_pcks', sum(valMetrics.values()))
        
        valMetrics['valLoss'] = float(valLoss)
        self.log('valLoss', float(valLoss), logger=False, prog_bar=True)
        
        mlflow.log_metrics(valMetrics, step=self.current_epoch)

    def configure_optimizers(self):
        configs :Dict[str, Any] = {}
        optimizer = self._GetOptimizer()
        scheduler = self._GetLrScheduler(optimizer)
        
        configs['optimizer'] = optimizer
        if scheduler is not None:
            configs['scheduler'] = scheduler
        if isinstance(scheduler, torch.optim.lr_scheduler.ReduceLROnPlateau):
            configs['monitor'] = 'valLoss'
        
        return configs

    def _GetLrScheduler(self, optimizer):
        if self.trainParameters["scheduler"] == 'StepLR':
            scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=15, gamma=0.8)
        if self.trainParameters["scheduler"] == 'Exponential':
            scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.1)
        if self.trainParameters["scheduler"] == 'ReduceOnPlateau':
            scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', patience=5, factor=0.5)
        else:
            scheduler = None
        return scheduler

    def _GetOptimizer(self):
        if self.trainParameters["optimizer"] == "Adam":
            optimizer = torch.optim.Adam(params=self.model.parameters(),
                                         lr=self.trainParameters["learningRate"],
                                         weight_decay=self.trainParameters["weightDecay"])
        elif self.trainParameters["optimizer"] == "AdamW":
            optimizer = torch.optim.AdamW(params=self.model.parameters(),
                                          lr=self.trainParameters["learningRate"],
                                          weight_decay=self.trainParameters["weightDecay"])
        elif self.trainParameters["optimizer"] == "RMSProp":
            optimizer = torch.optim.RMSprop(params=self.model.parameters(),
                                          lr=self.trainParameters["learningRate"],
                                          weight_decay=self.trainParameters["weightDecay"])
        elif self.trainParameters["optimizer"] == "Adagrad":
            optimizer = torch.optim.Adagrad(params=self.model.parameters(),
                                          lr=self.trainParameters["learningRate"],
                                          weight_decay=self.trainParameters["weightDecay"])
        else:
            optimizer = torch.optim.SGD(params=self.model.parameters(),
                                            lr=self.trainParameters["learningRate"],
                                            weight_decay=self.trainParameters["weightDecay"])
                                            
        return optimizer

    def get_progress_bar_dict(self):
        tqdmDict = super().get_progress_bar_dict()
        if 'v_num' in tqdmDict:
            del tqdmDict['v_num']
        return tqdmDict
