# Estimating Sheep Pain Level

The goal is to predict pain level from sheep faces. We use a similar approach as proposed in **Estimating Sheep Pain Level Using Facial Action Unit Detection.** Marwa Mahmoud et al

### Step 1: Predict landmark position ###
We developed a model that predicts position of landmarks given sheep image. We used an implementation of **Stacked Hourglass Networks for Human Pose Estimation.** by [Chris Rockwell](https://crockwell.github.io/). AnimalWeb dataset is used. See **AnimalWeb: A Large-Scale Hierarchical Dataset of Annotated Animal Faces.** Below we show model outputs. Training was done using PytorchLightning.
![This is an image](output.png)

### Step 2: Feature extraction before feeding to pain classifier ###
TODO: Crop regions around predicted landmarks then use Histogram of gradients 

### Step 3: Pain classifier ###
TODO: Build binary classifier
