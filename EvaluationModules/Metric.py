import torch
import torch.nn.functional as F


class PostProcessor:
    def __init__(self, reduction: str = 'last') -> None:
        self.reduction = reduction

    def __call__(self, predictedHeatmaps: torch.Tensor) -> torch.Tensor:
        reducedHeatmaps = self._ReduceHeatmaps(predictedHeatmaps)
        points = self._HeatmapToPoints(reducedHeatmaps)
        return points
        
    def _ReduceHeatmaps(self, predictedHeatmaps: torch.Tensor) -> torch.Tensor:
        if self.reduction == 'last':
            return predictedHeatmaps[0, -1]
        if self.reduction == 'mean':
            return predictedHeatmaps.mean(dim=1)[0]
        raise ValueError(f"Reduction mode {self.reduction} not supported")
    
    @staticmethod
    def _HeatmapToPoints(predictedHeatmaps: torch.Tensor) -> torch.Tensor:
        assert predictedHeatmaps.dim() == 3
        outputPoints = []
        for pointHeatmap in predictedHeatmaps:
            yCoord, xCoord = (pointHeatmap == torch.max(pointHeatmap)).nonzero()[0]
            outputPoints.append(torch.stack([xCoord, yCoord], dim=0))
        return torch.stack(outputPoints).float()


class PCK:
    def __init__(self, threshold: float = 9.0) -> None:
        self.threshold = threshold
        
    def __call__(self, predictedPoints: torch.Tensor, gtPoints: torch.Tensor) -> torch.Tensor:
        perJointDistance = F.mse_loss(predictedPoints, gtPoints, reduction='none').sum(dim=1)
        perJointScore = (perJointDistance <= self.threshold).float()
        return perJointScore


if __name__ == '__main__':
    PredHeatmaps = torch.rand((1, 4, 9, 400, 400))
    GtPoints = torch.randint(0, 400, size=(9, 2)).float() 
    PredPoints = PostProcessor()(PredHeatmaps)
    pckScore = PCK()(PredPoints, GtPoints)
    print(pckScore)