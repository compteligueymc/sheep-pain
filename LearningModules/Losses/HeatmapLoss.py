from typing import Any
from torch import nn

class HeatmapLoss(nn.Module):
    """
    loss for detection heatmap
    """
    def __init__(self, lossName: str = 'L2', reduction: str = 'sum', **kwargs: Any):
        super().__init__()
        self.lossFn = self._GetLossFn(lossName)
        self.reduction = reduction

    @staticmethod
    def _GetLossFn(lossName):
        if lossName == 'L2':
            lossFn = nn.MSELoss(reduction='none')
        elif lossName == 'L1':
            lossFn = nn.L1Loss(reduction='none')
        elif lossName == 'SmoothL1':
            lossFn = nn.SmoothL1Loss(reduction='none')
        return lossFn

    def forward(self, pred, gt):
        lossValue = self.lossFn(pred, gt)
        if self.reduction == 'mean':
            return lossValue.mean(dim=(3,2)).sum()
        if self.reduction == 'sum':
            return lossValue.sum(dim=(3,2)).mean(dim=(1,0))
        
        raise ValueError(f"Reduction {self.reduction} not known")