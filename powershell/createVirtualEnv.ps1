# Delete actual environment
try {
    Remove-Item -path env -recurse
    Write-Output "Delete environement"
}
catch {
    Write-Output "No existing environement found"
}
# Initialize the environement
Write-Output "Initialize the environement"
# python -m pip install --upgrade pip setuptools wheel --quiet
python -m venv env

# Install requirements
Write-Output "Install requirements"
python -m pip install --requirement requirements.txt

# Install pytorch
Write-Output "Install Pytorch"
python -m pip install  --no-deps `
    torch==1.10.1+cu113 torchvision==0.11.2+cu113 torchaudio===0.10.1+cu113 -f https://download.pytorch.org/whl/cu113/torch_stable.html